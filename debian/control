Source: python-aioitertools
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Julian Gilbey <jdg@debian.org>
Section: python
Priority: optional
Standards-Version: 4.7.0
Homepage: https://github.com/omnilib/aioitertools
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc <!nodoc>,
               flit,
               pybuild-plugin-pyproject,
               python3-all,
               python3-doc <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-sphinx-mdinclude <!nodoc>
Testsuite: autopkgtest-pkg-pybuild
Vcs-Git: https://salsa.debian.org/python-team/packages/python-aioitertools.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-aioitertools
Rules-Requires-Root: no
Description: Python itertools, builtins and more for AsyncIO
 This Python module shadows the standard library whenever possible to
 provide asynchronous versions of itertools, builtins and other familiar
 functions.  It's fully compatible with standard iterators and async
 iterators alike, providing a single unified, familiar interface for
 interacting with iterable objects.

Package: python3-aioitertools
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Multi-Arch: foreign
Description: ${source:Synopsis} (Python 3)
 ${source:Extended-Description}
 .
 This package provides the Python library.

Package: python-aioitertools-doc
Architecture: all
Section: doc
Depends: fonts-font-awesome,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Suggests: python3-sphinxemoji
Multi-Arch: foreign
Description: ${source:Synopsis} (documentation)
 ${source:Extended-Description}
 .
 This package contains HTML documentation.
Build-Profiles: <!nodoc>
